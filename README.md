# how_to_remove_php_extension_from_url

RewriteEngine On
RewriteRule something something.php [L]
http://example.com/something will be handled as if it was a request for something.php

To redirect all requests that are not a physical file to the same name but with .php:

RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule (.*) $1.php [L]